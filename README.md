# SSH Public Keys

A repository of public keys used by my controllers and bastion servers.

## Providing Access

A simple way to get one of these keys onto a machine is like so:

```bash
curl -sSL https://gitlab.com/abraxos/ssh-keys/-/raw/main/skynet-ansible-controller.pub | tr -d '\n' >> ~/.ssh/authorized_keys
```
